package com.app.carona.caronas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.app.carona.caronas.R
import com.app.carona.caronas.model.RideSolicitationItem
import com.app.carona.caronas.util.Firebase
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by teffe on 08/12/2017.
 */
class RideSolicitationsAdapter(context: Context, items: ArrayList<RideSolicitationItem>)
    : ArrayAdapter<RideSolicitationItem>(context, R.layout.item_ride_solicitation, items) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val layout = LayoutInflater.from(context)
                .inflate(R.layout.item_ride_solicitation, parent, false)

        initElements(layout, position)

        return layout
    }

    private fun initElements(layout: View?, position: Int) {
        val item = getItem(position)

        val civUserRequestingPhoto = layout?.findViewById<CircleImageView>(R.id.civ_rs_user_requesting_photo)
        val tvUserRequestingName = layout?.findViewById<TextView>(R.id.tv_rs_user_requesting_name)

        Picasso.with(civUserRequestingPhoto?.context)
                .load(item?.photoUri)
                .into(civUserRequestingPhoto)

        tvUserRequestingName?.text = item.username

        val ivRefuse = layout?.findViewById<ImageView>(R.id.iv_refuse)
        ivRefuse?.setOnClickListener { refuseSolicitation(item) }

        val ivAccept = layout?.findViewById<ImageView>(R.id.iv_accept)
        ivAccept?.setOnClickListener { acceptSolicitation(item) }
    }

    private fun acceptSolicitation(item: RideSolicitationItem?) {
        Firebase.groupRef
                .child(item?.groupName)
                .child("rides")
                .child(item?.rideId)
                .child("participants")
                .child(item?.userId)
                .setValue(true)

        removeSolicitation(item)
    }

    private fun refuseSolicitation(item: RideSolicitationItem?) {
        removeSolicitation(item)
    }

    private fun removeSolicitation(item: RideSolicitationItem?) {
        Firebase.groupRef
                .child(item?.groupName)
                .child("rides")
                .child(item?.rideId)
                .child("solicitations")
                .child(item?.userId)
                .removeValue()
        remove(item)
    }
}