package com.app.carona.caronas.entity

/**
 * Created by teffe on 25/11/2017.
 */
enum class ContactType(val description: String) {
    EMAIL("E-mail"),
    CELL_PHONE("Cell Phone")
}