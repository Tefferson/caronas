package com.app.carona.caronas.fragment


import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toolbar
import com.app.carona.caronas.R
import com.app.carona.caronas.adapter.ViewPagerAdapter
import com.app.carona.caronas.entity.User
import com.app.carona.caronas.model.GroupItem


/**
 * A simple [Fragment] subclass.
 */
class GroupDetailFragment : Fragment() {

    private var groupItem: GroupItem? = null
    private var layout: View? = null
    private val viewPager by lazy { layout?.findViewById<ViewPager>(R.id.viewpager) }
    private val tabLayout by lazy { layout?.findViewById<TabLayout>(R.id.tabs) }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        layout = inflater!!.inflate(R.layout.fragment_group_detail, container, false)

        groupItem = arguments.getSerializable("GROUP_ITEM") as GroupItem

        setupViewPager(viewPager!!);

        tabLayout?.setupWithViewPager(viewPager);

        return layout
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val isOwner = groupItem?.owner == User.currentUserId

        val adapter = ViewPagerAdapter(childFragmentManager)

        val bundle = Bundle()
        bundle.putString("GROUP_NAME", groupItem?.name)

        val groupRidesFragment = GroupRidesFragment()
        groupRidesFragment.arguments = bundle

        adapter.addFragment(groupRidesFragment, "Caronas")

        if (isOwner) {
            val groupSolicitationFragment = GroupSolicitationFragment()
            val bundle = Bundle()
            bundle.putSerializable("GROUP_ITEM", groupItem)
            groupSolicitationFragment.arguments = bundle

            adapter.addFragment(groupSolicitationFragment, "Solicitações")
        }

        viewPager.adapter = adapter
    }
}