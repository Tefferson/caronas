package com.app.carona.caronas.model

/**
 * Created by teffe on 08/12/2017.
 */
class RideSolicitationItem {
    var userId: String? = null
    var username: String? = null
    var photoUri: String? = null
    var groupName: String? = null
    var rideId: String? = null
}