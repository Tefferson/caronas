package com.app.carona.caronas.entity

/**
 * Created by teffe on 25/11/2017.
 */
class Address(var idUser: String,
              var zipCode: String,
              var description: String,
              var name: String) {
}