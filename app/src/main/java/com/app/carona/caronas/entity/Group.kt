package com.app.carona.caronas.entity

/**
 * Created by teffe on 25/11/2017.
 */
class Group(owner: User,
            name: String,
            members: ArrayList<User>,
            destinations: ArrayList<Address>) {
}