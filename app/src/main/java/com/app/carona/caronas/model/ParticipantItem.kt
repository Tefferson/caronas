package com.app.carona.caronas.model

/**
 * Created by teffe on 08/12/2017.
 */
class ParticipantItem {

    var photoUri: String? = null

    var username: String? = null

    var isOwner: Boolean = false

}