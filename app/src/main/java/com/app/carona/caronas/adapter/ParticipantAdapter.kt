package com.app.carona.caronas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.app.carona.caronas.R
import com.app.carona.caronas.model.ParticipantItem
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by teffe on 08/12/2017.
 */
class ParticipantAdapter(context: Context, items: ArrayList<ParticipantItem>) : ArrayAdapter<ParticipantItem>(context, R.layout.item_participant, items) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val layout = LayoutInflater.from(context)
                .inflate(R.layout.item_participant, parent, false)

        initElements(layout, position)

        return layout
    }

    private fun initElements(layout: View?, position: Int) {

        val item = getItem(position)

        val civParticipantPhoto = layout?.findViewById<CircleImageView>(R.id.civ_ip_participant_photo)
        val tvParticipantName = layout?.findViewById<TextView>(R.id.tv_ip_participant_name)

        Picasso.with(civParticipantPhoto?.context)
                .load(item.photoUri)
                .into(civParticipantPhoto)

        tvParticipantName?.text = item.username

        if(item.isOwner) {
            layout?.setBackgroundColor(context.resources.getColor(R.color.colorPrimaryLight))
        }
    }
}