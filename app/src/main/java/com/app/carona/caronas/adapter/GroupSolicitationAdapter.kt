package com.app.carona.caronas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.app.carona.caronas.R
import com.app.carona.caronas.model.GroupSolicitationItem
import com.app.carona.caronas.util.Firebase
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by teffe on 05/12/2017.
 */
class GroupSolicitationAdapter(context: Context, items: ArrayList<GroupSolicitationItem>)
    : ArrayAdapter<GroupSolicitationItem>(context, R.layout.item_group_solicitation, items) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val layout = LayoutInflater.from(context)
                .inflate(R.layout.item_group_solicitation, parent, false)

        initElements(layout, position)

        return layout
    }

    private fun initElements(layout: View?, position: Int) {
        val item = getItem(position)

        val civUser = layout?.findViewById<CircleImageView>(R.id.civ_user)
        Picasso.with(context)
                .load(item.photoUri)
                .into(civUser)

        val tvElapsedTime = layout?.findViewById<TextView>(R.id.tv_elapsed_time)
        tvElapsedTime?.text = item.elapsedTime

        val tvDisplayName = layout?.findViewById<TextView>(R.id.tv_display_name)
        tvDisplayName?.text = item.name

        val ivAccept = layout?.findViewById<ImageView>(R.id.iv_accept)
        ivAccept?.setOnClickListener {
            acceptSolicitation(item)
        }

        val ivClose = layout?.findViewById<ImageView>(R.id.iv_close)
        ivClose?.setOnClickListener {
            Toast.makeText(context, "${item.name} foi rejeitado pelo grupo seleto", Toast.LENGTH_SHORT).show()
        }
    }

    private fun acceptSolicitation(item: GroupSolicitationItem?) {

        remove(item)

        Firebase.groupRef
                .child(item?.groupName)
                .child("solicitations")
                .child(item?.userId)
                .removeValue()

        val newMember = Firebase.groupRef
                .child(item?.groupName)
                .child("members")
                .child(item?.userId)
                .setValue(true)

        val addedGroup = Firebase
                .userRef
                .child(item?.userId)
                .child("groups")
                .child(item?.groupName)
                .setValue(true)

        Toast.makeText(context, "${item?.name} foi aceito no grupo ${item?.groupName}", Toast.LENGTH_SHORT).show()
    }
}