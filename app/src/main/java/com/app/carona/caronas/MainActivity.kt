package com.app.carona.caronas

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.app.carona.caronas.adapter.MainPagerAdapter
import com.app.carona.caronas.entity.User
import com.app.carona.caronas.enum.MainPager
import com.app.carona.caronas.util.BottomNavigationUtil
import com.app.carona.caronas.util.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener


class MainActivity : AppCompatActivity(),
        View.OnClickListener,
        ViewPager.OnPageChangeListener,
        BottomNavigationView.OnNavigationItemSelectedListener {

    private val vpMain by lazy { findViewById<ViewPager>(R.id.vp_main) }
    private val bottomNavigation by lazy { findViewById<BottomNavigationView>(R.id.bottom_navigation) }
    private val adapter by lazy { MainPagerAdapter(this, vpMain, this) }

    override fun onCreate(savedInstanceState: Bundle?) {

        //utilizar quando for em device físico
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w = window
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        }*/

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (FirebaseAuth.getInstance().currentUser == null) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            initUser()
            initElements()
        }
    }


    private fun initUser() {
        val self = this
        val postListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val user = dataSnapshot.getValue(User::class.java)
                if (user == null) {
                    saveNewUser()
                } else {
                    User.currentUser = user
                    Toast.makeText(self, user.name, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        }

        User.currentUserId = FirebaseAuth.getInstance()!!.uid

        Firebase.userRef.child(User.currentUserId).addValueEventListener(postListener)
    }

    private fun saveNewUser() {
        val user = User().apply {
            name = FirebaseAuth.getInstance().currentUser?.displayName
            photoUri = FirebaseAuth.getInstance().currentUser?.photoUrl.toString()
            if (name.isNullOrEmpty() || name.isNullOrBlank()) {
                name = "sem nome"
                val request = UserProfileChangeRequest.Builder().setDisplayName(name).build()
                FirebaseAuth.getInstance().currentUser?.updateProfile(request)
            }
            isActive = true
        }

        User.currentUser = user

        Firebase.userRef.child(User.currentUserId).setValue(user)
    }

    override fun onClick(viewItem: View?) {
        when (viewItem?.id) {
        }
    }

    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {
        val itemId = MainPager.values()[position].itemId
        if (bottomNavigation.selectedItemId != itemId) {
            bottomNavigation.selectedItemId = itemId
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val idx = BottomNavigationUtil.getSelectedOrder(bottomNavigation, item)
        if (item.itemId != bottomNavigation.selectedItemId && vpMain.currentItem != idx) {
            vpMain.currentItem = idx
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu?.findItem(R.id.search)?.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.isSubmitButtonEnabled = true
        searchView.setOnQueryTextListener(adapter)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.search -> {
                onSearchRequested()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun initElements() {
        vpMain.adapter = adapter
        vpMain.offscreenPageLimit = Int.MAX_VALUE
        vpMain.addOnPageChangeListener(this)

        bottomNavigation.setOnNavigationItemSelectedListener(this)
    }

    private fun btLogoutOnclick() {
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
