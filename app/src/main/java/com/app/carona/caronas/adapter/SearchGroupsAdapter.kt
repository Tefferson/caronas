package com.app.carona.caronas.adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.app.carona.caronas.R
import com.app.carona.caronas.entity.User
import com.app.carona.caronas.model.SearchGroupItem
import com.app.carona.caronas.util.Firebase
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

/**
 * Created by teffe on 04/12/2017.
 */
class SearchGroupsAdapter(context: Context, items: ArrayList<SearchGroupItem>)
    : ArrayAdapter<SearchGroupItem>(context, R.layout.item_search_group, items), View.OnClickListener {

    override fun onClick(view: View?) {
        val tvName = view?.findViewById<TextView>(R.id.tv_ig_group_name)
        AlertDialog.Builder(context)
                .setMessage("Deseja participar do grupo ${tvName?.text}?")
                .setTitle("Pesquisa de grupos")
                .setNegativeButton(R.string.cancelar, { dialogInterface, _ ->
                    dialogInterface.dismiss()
                })
                .setPositiveButton(R.string.sim, { _, _ ->
                    joinGroup(tvName?.text.toString())
                })
                .create()
                .show()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layout = LayoutInflater.from(context)
                .inflate(R.layout.item_search_group, parent, false)

        initElements(layout, position)

        return layout
    }

    private fun initElements(layout: View, position: Int) {
        val item = getItem(position)
        val tvGroupName = layout.findViewById<TextView>(R.id.tv_ig_group_name)
        tvGroupName.text = item.name

        val tvGroupDescription = layout.findViewById<TextView>(R.id.tv_ig_group_description)
        tvGroupDescription.text = item.description

        val tvPeopleCount = layout.findViewById<TextView>(R.id.tv_people_count)
        tvPeopleCount.text = item.peopleCount.toString()

        val civGroupImage = layout.findViewById<CircleImageView>(R.id.civ_ig_group_image)
        Picasso.with(context)
                .load(item.imageUri)
                .into(civGroupImage)

        layout.setOnClickListener(this)
    }

    override fun add(`object`: SearchGroupItem?) {
        val position = getPosition(`object`)
        if (position < 0) {
            super.add(`object`)
        }
    }

    private fun joinGroup(groupName: String) {
        val solicitation = Firebase.groupRef.child(groupName).child("solicitations").child(User.currentUserId)
        solicitation.child("date").setValue(Date())
    }
}