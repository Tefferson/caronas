package com.app.carona.caronas.model

import java.io.Serializable

/**
 * Created by teffe on 07/12/2017.
 */
class GroupDetailRidesItem {

    var rideId: String? = null

    var userId: String? = null

    var userName: String? = null

    var destiny: String? = null

    var origin: String? = null

    var arrivalHour: Int? = null

    var arrivalMinute: Int? = null

    var groupName: String? = null

    var participants: HashMap<String, Boolean>? = null
}