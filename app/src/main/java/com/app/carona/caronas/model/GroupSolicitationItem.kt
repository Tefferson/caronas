package com.app.carona.caronas.model

/**
 * Created by teffe on 05/12/2017.
 */
class GroupSolicitationItem {
    var name: String? = null
    var userId: String? = null
    var groupName: String? = null
    var elapsedTime: String? = null
    var photoUri: String? = null
}