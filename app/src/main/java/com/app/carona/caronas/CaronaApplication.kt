package com.app.carona.caronas

import android.app.Application
import com.google.firebase.FirebaseApp

/**
 * Created by teffe on 26/11/2017.
 */
class CaronaApplication : Application() {

    override fun onCreate() {
        FirebaseApp.initializeApp(this)
        super.onCreate()
    }
}