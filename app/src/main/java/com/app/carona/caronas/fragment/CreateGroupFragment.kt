package com.app.carona.caronas.fragment


import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.app.carona.caronas.R
import com.app.carona.caronas.entity.User
import com.app.carona.caronas.util.Firebase
import de.hdodenhof.circleimageview.CircleImageView
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.app.carona.caronas.enum.RequestCodes
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.database.DatabaseReference
import com.squareup.picasso.Picasso


class CreateGroupFragment : Fragment(), View.OnClickListener {

    private var civGroupImage: CircleImageView? = null

    private var etGroupName: EditText? = null

    private var etGroupDescription: EditText? = null

    private var btSave: Button? = null

    /**
     * URI da imagem selecionada para o grupo.
     */
    private var groupImageUri: Uri? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layout = inflater!!.inflate(R.layout.fragment_create_group, container, false)
        initElements(layout)
        return layout
    }

    private fun initElements(layout: View) {

        layout.run {
            civGroupImage = findViewById(R.id.civ_cg_group_image)
            etGroupName = findViewById(R.id.et_cg_group_name)
            etGroupDescription = findViewById(R.id.et_cg_group_description)
            btSave = findViewById(R.id.bt_save)
        }

        civGroupImage?.setOnClickListener(this)
        btSave?.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.bt_save -> createGroup()
            R.id.civ_cg_group_image -> chooseGroupImage()
        }
    }

    private fun chooseGroupImage() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"

        startActivityForResult(intent, RequestCodes.IMAGE.code)
    }

    private fun createGroup() {

        val chave = etGroupName?.text.toString()

        // Vincula o usuário com o grupo.
        Firebase.userRef
                .child(User.currentUserId)
                .child("groups")
                .child(chave)
                .setValue(true)

        // Cria o grupo.
        val group = Firebase.groupRef.child(chave)
        group.child("name").setValue(chave)
        group.child("description").setValue(etGroupDescription?.text.toString())
        group.child("owner").setValue(User.currentUserId)
        group.child("members").child(User.currentUserId).setValue(true)
        if (groupImageUri != null) {
            group.child("imageUri").setValue("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT2VbnvuhEKdmmr0UNUR1X-Wx3snF1DUfks66g4kqb2xdm9JO6J", DatabaseReference.CompletionListener { databaseError, databaseReference ->
                if (databaseError == null) {
                    val storageReference = FirebaseStorage.getInstance()
                            .getReference(FirebaseAuth.getInstance().currentUser!!.uid)
                            .child(chave)
                            .child(groupImageUri!!.lastPathSegment)

                    putImageInStorage(storageReference, groupImageUri!!, chave)
                } else {
                    Log.w("qualquer coisa", "Unable to write message to database.",
                            databaseError.toException())
                }
            })
        }

        group.push()

        Toast.makeText(activity, "Grupo criado", Toast.LENGTH_SHORT).show()
        closeKeyboard()
        activity.fragmentManager.popBackStackImmediate()
    }

    private fun putImageInStorage(storageReference: StorageReference, groupImageUri: Uri, chave: String) {

        storageReference.putFile(groupImageUri).addOnCompleteListener({ task ->
            if (task.isSuccessful()) {

                Firebase.groupRef.child(chave)
                        .child("imageUri")
                        .setValue(task.getResult().getMetadata()?.getDownloadUrl().toString())
            } else {
                val tag = "TAG"
                Log.w(tag, "Image upload task was not successful.",
                        task.getException());
            }
        })
    }

    private fun trySaveDestination(etDestination: EditText?) {
        if (!etDestination?.text.isNullOrEmpty()) {
            val destination = Firebase.addressRef
                    .child(etGroupName?.text.toString())
                    .child(etDestination?.text.toString())

            destination.setValue(true)
            destination.push()
        }
    }

    private fun closeKeyboard() {
        if (activity.currentFocus != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity.currentFocus.windowToken, 0)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {

        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RequestCodes.IMAGE.code) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                if (data != null) {
                    groupImageUri = data.data


                    if (groupImageUri!!.lastPathSegment.startsWith("gs://")) {
                        val storageReference = FirebaseStorage.getInstance()
                                .getReferenceFromUrl(groupImageUri!!.lastPathSegment)
                        storageReference.downloadUrl.addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                val downloadUrl = task.result.toString()
                                Picasso.with(civGroupImage!!.context)
                                        .load(downloadUrl)
                                        .into(civGroupImage)
                            }
                        }
                    } else {
                        Picasso.with(civGroupImage!!.context)
                                .load(groupImageUri)
                                .into(civGroupImage)
                    }

                }
            }
        }
    }

}
