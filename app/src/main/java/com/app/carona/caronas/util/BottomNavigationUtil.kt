package com.app.carona.caronas.util

import android.support.design.widget.BottomNavigationView
import android.view.MenuItem

/**
 * Created by tefferson.guterres on 30/11/2017.
 */
object BottomNavigationUtil {
    fun getSelectedOrder(bottomNavigationView: BottomNavigationView, menuItem: MenuItem): Int {
        val menu = bottomNavigationView.menu
        for (i in 0..(menu.size() - 1)) {
            val item = menu.getItem(i)
            if (item.itemId == menuItem.itemId) {
                return i
            }
        }
        return -1
    }
}