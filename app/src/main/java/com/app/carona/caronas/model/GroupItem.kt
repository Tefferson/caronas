package com.app.carona.caronas.model

import java.io.Serializable

/**
 * Created by Jackelyn on 12/3/2017.
 */
class GroupItem : Serializable {

    var name: String? = null
    var description: String? = null
    var owner: String? = null
    var notificationsCount: Long = 0
    var membersCount: Long = 0
    var imageUri: String? = null

    override fun equals(other: Any?): Boolean {
        return (other as GroupItem).name == name
    }
}