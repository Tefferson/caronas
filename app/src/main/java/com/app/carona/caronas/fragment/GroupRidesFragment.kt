package com.app.carona.caronas.fragment


import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView
import android.widget.Toast

import com.app.carona.caronas.R
import com.app.carona.caronas.adapter.GroupDetailRidesAdapter
import com.app.carona.caronas.entity.User
import com.app.carona.caronas.model.GroupDetailRidesItem
import com.app.carona.caronas.util.Firebase
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError


/**
 * A simple [Fragment] subclass.
 */
class GroupRidesFragment : Fragment(), View.OnClickListener {

    var layout: View? = null
    var groupName: String? = null

    private val lvGroupDetailRides by lazy { layout?.findViewById<ListView>(R.id.lv_group_detail_rides) }
    private val btOfferRide by lazy { layout?.findViewById<Button>(R.id.bt_offer_ride) }

    private val adapter by lazy { GroupDetailRidesAdapter(activity, ArrayList<GroupDetailRidesItem>()) }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        layout = inflater!!.inflate(R.layout.fragment_group_rides, container, false)

        groupName = arguments.getString("GROUP_NAME")

        initElements()

        return layout
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.bt_offer_ride -> openCreateRideOfferFragment()
        }
    }

    private fun requestSeat(item: GroupDetailRidesItem) {
        if (item.userId != User.currentUserId) {
            AlertDialog.Builder(context)
                    .setTitle("Solicitar Carona")
                    .setMessage("Deseja solicitar carona para ${item.destiny}?")
                    .setPositiveButton(R.string.sim, { _, _ -> rideSolicitation(item) })
                    .setNegativeButton("Não", { dialogInterface, _ -> dialogInterface.dismiss() })
                    .create()
                    .show()
        }
    }

    private fun rideSolicitation(item: GroupDetailRidesItem) {
        Firebase.groupRef
                .child(groupName)
                .child("rides")
                .child(item.rideId)
                .child("solicitations")
                .child(User.currentUserId)
                .setValue(true)

        Toast.makeText(context, "Solicitação de Carona Enviada", Toast.LENGTH_SHORT).show()
    }

    private fun openCreateRideOfferFragment() {
        val bundle = Bundle()
        bundle.putString("GROUP_NAME", groupName)
        val fragment = CreateRideOfferFragment()
        fragment.arguments = bundle
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.view_group_rides, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun initElements() {
        lvGroupDetailRides?.adapter = adapter
        lvGroupDetailRides?.setOnItemClickListener { adapter, _, position, _ ->
            val item = adapter.getItemAtPosition(position) as GroupDetailRidesItem
            if (item.userId == User.currentUserId) {
                openRideSolicitationsFragment(item)
            } else {
                requestSeat(item)
            }
        }
        loadRides()
        btOfferRide?.setOnClickListener(this)
    }

    private fun openRideSolicitationsFragment(item: GroupDetailRidesItem) {
        val bundle = Bundle()
        bundle.putString("GROUP_NAME", groupName)
        bundle.putString("RIDE_ID", item.rideId)
        val fragment = RideSolicitationsFragment()
        fragment.arguments = bundle
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.view_group_rides, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun loadRides() {
        val query = Firebase.groupRef.child(groupName).child("rides").orderByValue()
        query.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {}
            override fun onChildAdded(dataSnapshot: DataSnapshot?, p1: String?) {
                val item = dataSnapshot?.getValue(GroupDetailRidesItem::class.java)
                item?.rideId = dataSnapshot?.key
                item?.groupName = groupName
                adapter.add(item)
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot?) {}
        })
    }

}
