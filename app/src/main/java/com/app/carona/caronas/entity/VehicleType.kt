package com.app.carona.caronas.entity

/**
 * Created by teffe on 25/11/2017.
 */
enum class VehicleType(val description: String) {
    CAR("Car"),
    MOTORCYCLE("Motorcycle"),
}