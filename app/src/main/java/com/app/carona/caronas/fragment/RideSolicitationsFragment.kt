package com.app.carona.caronas.fragment


import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast

import com.app.carona.caronas.R
import com.app.carona.caronas.adapter.RideSolicitationsAdapter
import com.app.carona.caronas.model.RideSolicitationItem
import com.app.carona.caronas.util.Firebase
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener


/**
 * A simple [Fragment] subclass.
 */
class RideSolicitationsFragment : Fragment() {

    private var groupName: String? = null
    private var rideId: String? = null
    private var layout: View? = null
    private val lvRideSolicitations by lazy { layout?.findViewById<ListView>(R.id.lv_ride_solicitations) }
    private val adapter by lazy { RideSolicitationsAdapter(context, ArrayList<RideSolicitationItem>()) }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        layout = inflater!!.inflate(R.layout.fragment_ride_solicitations, container, false)

        groupName = arguments.getString("GROUP_NAME")
        rideId = arguments.getString("RIDE_ID")

        initElements()

        return layout
    }

    private fun initElements() {
        lvRideSolicitations?.adapter = adapter
        loadRideSolicitations()
    }

    private fun loadRideSolicitations() {
        val query = Firebase.groupRef
                .child(groupName)
                .child("rides")
                .child(rideId)
                .child("solicitations")
                .orderByValue()

        query.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {}
            override fun onChildAdded(dataSnapshot: DataSnapshot?, p1: String?) {
                loadUserInfo(dataSnapshot?.key)
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot?) {}
        })
    }

    private fun loadUserInfo(userId: String?) {
        val query = Firebase.userRef.child(userId).orderByValue()
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val item = RideSolicitationItem()
                item.username = dataSnapshot?.child("name")?.getValue(String::class.java)
                item.photoUri = dataSnapshot?.child("photoUri")?.getValue(String::class.java)
                item.groupName = groupName
                item.rideId = rideId
                item.userId = userId
                adapter.add(item)
            }
        })
    }
}
