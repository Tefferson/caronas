package com.app.carona.caronas.util

import com.app.carona.caronas.entity.User
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

/**
 * Created by teffe on 26/11/2017.
 */
object Firebase {

    private val address = "address"
    private val contact = "contact"
    private val group = "group"
    private val solicitation = "solicitation"
    private val user = "user"

    val instance: FirebaseDatabase
        get() {
            return FirebaseDatabase.getInstance()
        }

    val ref: DatabaseReference
        get() {
            return instance.reference
        }

    val addressRef: DatabaseReference
        get() {
            return ref.child(address)
        }

    val contactRef: DatabaseReference
        get() {
            return ref.child(contact)
        }

    val groupRef: DatabaseReference
        get() {
            return ref.child(group)
        }

    val solicitationRef: DatabaseReference
        get() {
            return ref.child(solicitation)
        }

    val userRef: DatabaseReference
        get() {
            return ref.child(user)
        }
}