package com.app.carona.caronas.entity

/**
 * Created by teffe on 25/11/2017.
 */
class Contact(var idUser: String,
              var contactType: ContactType,
              var description: String) {
}