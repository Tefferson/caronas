package com.app.carona.caronas.model

import java.io.Serializable

/**
 * Created by teffe on 04/12/2017.
 */
class SearchGroupItem : Serializable {
    var name: String? = null
    var owner: String? = null
    var imageUri: String? = null
    var description: String? = null
    var peopleCount: Long = 0

    override fun equals(other: Any?): Boolean {
        return (other as SearchGroupItem).name == name
    }
}