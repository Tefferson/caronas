package com.app.carona.caronas.entity

/**
 * Created by teffe on 25/11/2017.
 */
class RideOffer(author: User,
                vehicle: Vehicle,
                seatsAvailable: Int,
                seatsOccupied: Int,
                participants: ArrayList<User>,
                destination: Address,
                arrivalHour: String,
                observation: String,
                isActive: Boolean) {
}