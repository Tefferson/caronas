package com.app.carona.caronas.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.app.carona.caronas.R
import com.app.carona.caronas.entity.User
import com.app.carona.caronas.model.RideItem
import com.app.carona.caronas.util.Firebase
import java.util.*
import android.app.TimePickerDialog
import android.text.format.DateFormat
import android.widget.TextView
import com.app.carona.caronas.util.TimeUtils
import java.text.SimpleDateFormat


/**
 * A simple [Fragment] subclass.
 */
class CreateRideOfferFragment : Fragment(), View.OnClickListener {

    private var groupName: String? = null
    private var layout: View? = null

    private val etRideDestiny by lazy { layout?.findViewById<EditText>(R.id.et_cr_ride_destiny) }
    private val etRideOrigin by lazy { layout?.findViewById<EditText>(R.id.et_cr_ride_origin) }
    private val etSeatsNumber by lazy { layout?.findViewById<EditText>(R.id.et_cr_seats_number) }
    private val tvRideTime by lazy { layout?.findViewById<TextView>(R.id.tv_cr_ride_time) }
    private val btPickRideTime by lazy { layout?.findViewById<Button>(R.id.bt_cr_pick_ride_time) }
    private val btCreateRide by lazy { layout?.findViewById<Button>(R.id.bt_create_ride_offer) }

    private var rideHour: Int? = null;
    private var rideMinute: Int? = null;

    private val RIDE_DEFAULT_HOUR = 8
    private val RIDE_DEFAULT_MINUTE = 30

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        layout = inflater!!.inflate(R.layout.fragment_create_ride_offer, container, false)

        groupName = arguments.getString("GROUP_NAME")

        initElements()

        return layout
    }

    private fun initElements() {
        btPickRideTime?.setOnClickListener(this)
        btCreateRide?.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.bt_create_ride_offer -> validateFieldsAndCreateRideOffer()
            R.id.bt_cr_pick_ride_time -> openTimePickerDialog()
        }
    }

    private fun openTimePickerDialog() {

        val timePickerDialog = TimePickerDialog(
                activity,
                TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->

                    rideHour = selectedHour
                    rideMinute = selectedMinute

                    tvRideTime?.text = TimeUtils.format(activity.applicationContext, rideHour, rideMinute)
                },
                rideHour ?: RIDE_DEFAULT_HOUR,
                rideMinute ?: RIDE_DEFAULT_MINUTE,
                DateFormat.is24HourFormat(activity))

        timePickerDialog.setTitle("Horário de Chegada")
        timePickerDialog.show()
    }

    private fun validateFieldsAndCreateRideOffer() {
        if (isValidRideOffer()) {
            createRideOffer()
            Toast.makeText(activity, "Oferta de Carona Criada", Toast.LENGTH_SHORT).show()
            closeKeyboard()
            fragmentManager.popBackStackImmediate()
        }
    }

    private fun isValidRideOffer(): Boolean {

        if(etRideDestiny?.text.isNullOrBlank()
            || etRideOrigin?.text.isNullOrBlank()
            || etSeatsNumber?.text.isNullOrBlank() || Integer.parseInt(etSeatsNumber?.text.toString()) <= 0
            || rideHour == null || rideMinute == null) {

            return false;
        }

        return true;
    }

    private fun createRideOffer() {

        val rideOffer = RideItem()

        rideOffer.apply {
            userId = User.currentUserId
            userName = User.currentUser?.name
            seatsNumber = etSeatsNumber?.text.toString().toInt()
            destiny = etRideDestiny?.text.toString()
            origin = etRideOrigin?.text.toString()
            arrivalHour = rideHour
            arrivalMinute = rideMinute
        }

        val newRideOffer = Firebase.groupRef
                .child(groupName)
                .child("rides")
                .push()

        newRideOffer.setValue(rideOffer)
    }

    private fun closeKeyboard() {
        if (activity.currentFocus != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity.currentFocus.windowToken, 0)
        }
    }

}