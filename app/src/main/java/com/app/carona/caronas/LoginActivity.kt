package com.app.carona.caronas

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.facebook.CallbackManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private val etEmail by lazy { findViewById<EditText>(R.id.et_email) }
    private val etPassword by lazy { findViewById<EditText>(R.id.et_password) }
    private val btSignIn by lazy { findViewById<Button>(R.id.bt_sign_in) }
    private val btSignUp by lazy { findViewById<Button>(R.id.bt_create_account) }
    private val signInButton by lazy { findViewById<SignInButton>(R.id.sign_in_button) }

    private val rcSignIn = 42
    private val mAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initElements()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            rcSignIn -> {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)
            }
        }
    }

    override fun onClick(viewItem: View?) {
        when (viewItem?.id) {
            R.id.bt_sign_in -> btSignInOnClick()
            R.id.bt_create_account -> btSignUpOnClick()
            R.id.sign_in_button -> signIn()
        }
    }

    private fun sendEmailVerification() {
        val currentUser = mAuth.currentUser
        currentUser?.sendEmailVerification()?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Toast.makeText(this, R.string.confirmation_email_sent, Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, R.string.fail_sending_confirmation_email, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun verifyEmailConfirmation() {
        val isEmailVerified = mAuth.currentUser?.isEmailVerified
        if (isEmailVerified!!) {
            Toast.makeText(this, R.string.login_succeeded, Toast.LENGTH_SHORT).show()
            startMainActivity()
        } else {
            Toast.makeText(this, R.string.mail_pending_confirmation, Toast.LENGTH_SHORT).show()
        }
    }

    private fun initElements() {
        btSignIn.setOnClickListener(this)
        btSignUp.setOnClickListener(this)
        signInButton.setOnClickListener(this)
        signInButton.setSize(SignInButton.SIZE_STANDARD)
    }

    private fun signIn() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        startActivityForResult(mGoogleSignInClient.signInIntent, rcSignIn);
    }

    private fun btSignUpOnClick() {
        val email = etEmail.text.toString()
        val password = etPassword.text.toString()
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                sendEmailVerification()
            } else {
                Toast.makeText(this, R.string.fail_creating_user, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun btSignInOnClick() {
        val email = etEmail.text.toString()
        val password = etPassword.text.toString()
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                verifyEmailConfirmation()
            } else {
                Toast.makeText(this, R.string.login_fail, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>?) {
        try {
            val account = completedTask?.getResult(ApiException::class.java)
            firebaseAuthWithGoogle(account)
        } catch (ex: ApiException) {
            Toast.makeText(this, R.string.login_fail, Toast.LENGTH_SHORT).show()
        }

    }

    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount?) {
        val credential = GoogleAuthProvider.getCredential(account?.idToken, null)
        mAuth.signInWithCredential(credential).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                Toast.makeText(this, R.string.authenticated, Toast.LENGTH_SHORT).show()
                startMainActivity()
            } else {
                Toast.makeText(this, R.string.authentication_fail, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun startMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}