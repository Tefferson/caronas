package com.app.carona.caronas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.app.carona.caronas.R
import com.app.carona.caronas.model.RideItem
import com.app.carona.caronas.util.Firebase
import com.app.carona.caronas.util.TimeUtils
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.sql.Time

/**
 * Created by tefferson.guterres on 06/12/2017.
 */
class RideAdapter(context: Context, items: ArrayList<RideItem>) : ArrayAdapter<RideItem>(context, R.layout.item_my_rides, items) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val layout = LayoutInflater.from(context)
                .inflate(R.layout.item_my_rides, parent, false)

        initElements(layout, position)

        return layout
    }

    private fun initElements(layout: View?, position: Int) {

        val item = getItem(position)

        val tvRideDestiny = layout?.findViewById<TextView>(R.id.tv_igdr_ride_destiny)
        val tvRideOrigin = layout?.findViewById<TextView>(R.id.tv_imr_ride_origin)
        val tvRideIntendedArrivalTime = layout?.findViewById<TextView>(R.id.tv_imr_ride_intended_arrival_time)
        val tvPeopleCount = layout?.findViewById<TextView>(R.id.tv_people_count)

        tvRideDestiny?.text = item.destiny
        tvRideOrigin?.text = item.origin
        tvRideIntendedArrivalTime?.text = TimeUtils.format(tvRideIntendedArrivalTime?.context, item.arrivalHour, item.arrivalMinute)

        val query = Firebase.groupRef.child(item.groupName).child("rides").child(item.rideId).orderByValue()
        query.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val rideData = dataSnapshot?.getValue(RideItem::class.java)
                val seatsOccupied = rideData?.participants?.count() ?: 0
                tvPeopleCount?.text = "$seatsOccupied/${rideData?.seatsNumber}"
            }
        })
    }
}