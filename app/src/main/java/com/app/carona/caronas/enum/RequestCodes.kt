package com.app.carona.caronas.enum

/**
 * Created by Jackelyn on 12/8/2017.
 */
enum class RequestCodes(val code: Int) {
    IMAGE(2)
}