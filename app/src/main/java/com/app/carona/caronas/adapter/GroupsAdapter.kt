package com.app.carona.caronas.adapter

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.app.carona.caronas.R
import com.app.carona.caronas.entity.User
import com.app.carona.caronas.fragment.GroupDetailFragment
import com.app.carona.caronas.model.GroupItem
import com.app.carona.caronas.util.Firebase
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by Jackelyn on 12/3/2017.
 */
class GroupsAdapter(context: Context, items: List<GroupItem>, private val activity: AppCompatActivity)
    : ArrayAdapter<GroupItem>(context, R.layout.item_group, items) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layout = LayoutInflater.from(context)
                .inflate(R.layout.item_group, parent, false)

        initElements(layout, position)

        return layout
    }

    private fun initElements(layout: View, position: Int) {
        val item = getItem(position)

        val tvName = layout.findViewById<TextView>(R.id.tv_ig_group_name)
        val tvDescription = layout.findViewById<TextView>(R.id.tv_ig_group_description)
        val tvNotificationsCount = layout.findViewById<TextView>(R.id.tv_ig_group_notifications_count)
        val civGroupName = layout.findViewById<CircleImageView>(R.id.civ_ig_group_image)

        // Carrega nome do grupo...
        tvName.text = item.name

        // Carrega descrição do grupo...
        tvDescription.text = item.description

        // Carrega quantidade de notificações...
        val isOwner = item.owner == User.currentUserId
        if (isOwner) {
            layout.setBackgroundColor(context.resources.getColor(R.color.colorPrimaryLight))

            if (item.notificationsCount > 0) {
                tvNotificationsCount.visibility = View.VISIBLE
                tvNotificationsCount.text = item.notificationsCount.toString()
            }
        }

        //Carrega imagem do grupo...
        if (item.imageUri != null) {

            Picasso.with(civGroupName.context)
                    .load(item.imageUri)
                    .into(civGroupName)
        }

        layout.setOnClickListener({ showGroupDetail(item) })

        layout.setOnLongClickListener {
            if (isOwner) {
                excludeGroup(item?.name!!)
            } else {
                exitGroup(item?.name!!)
            }
            true
        }

        val tvPeopleCount = layout.findViewById<TextView>(R.id.tv_people_count)
        tvPeopleCount.text = item.membersCount.toString()
    }

    private fun showGroupDetail(item: GroupItem?) {
        val bundle = Bundle()
        bundle.putSerializable("GROUP_ITEM", item)
        val fragment = GroupDetailFragment()
        fragment.arguments = bundle
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.view_groups, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun exitGroup(groupName: String) {
        AlertDialog.Builder(context)
                .setTitle("Grupo $groupName")
                .setMessage("Deseja sair do grupo?")
                .setPositiveButton("sim", { _, _ ->
                    Firebase.groupRef.child(groupName).child("members").child(User.currentUserId).removeValue()
                    Firebase.userRef.child(User.currentUserId).child("groups").child(groupName).removeValue()
                    Toast.makeText(context, "Você saiu do grupo $groupName", Toast.LENGTH_SHORT).show()
                })
                .setNegativeButton("não", { dialogInterface, _ ->
                    dialogInterface.dismiss()
                })
                .create()
                .show()
    }

    private fun excludeGroup(groupName: String) {
        AlertDialog.Builder(context)
                .setTitle("Grupo $groupName")
                .setMessage("Deseja excluir o grupo?")
                .setPositiveButton("sim", { _, _ ->
                    removeGroupFromUsers(groupName)
                    Toast.makeText(context, "Você excluiu o grupo $groupName", Toast.LENGTH_SHORT).show()
                })
                .setNegativeButton("não", { dialogInterface, _ ->
                    dialogInterface.dismiss()
                })
                .create()
                .show()
    }

    private fun removeGroupFromUsers(groupName: String) {
        val query = Firebase.groupRef.child(groupName).child("members").orderByValue()
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                for (child in dataSnapshot?.children!!) {
                    val userId = child.key
                    Firebase.userRef.child(userId).child("groups").child(groupName).removeValue()
                }
                Firebase.groupRef.child(groupName).removeValue()
                Firebase.addressRef.child(groupName).removeValue()
            }

        })
    }

    fun add(groupName: String) {
        val query = Firebase.groupRef.child(groupName).orderByValue()
        query.addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot?) {

                        val notificationsCount = snapshot?.child("solicitations")?.childrenCount
                        val peopleCount = snapshot?.child("members")?.childrenCount

                        var item = GroupItem()
                        item.name = snapshot?.key.toString()
                        item.membersCount = peopleCount!!

                        val position = getPosition(item)
                        if (position >= 0) {
                            var item = getItem(position)
                            item.notificationsCount = notificationsCount!!
                            notifyDataSetChanged()
                        } else {
                            val owner = snapshot?.child("owner")?.getValue(String::class.java)
                            if (owner != null) {
                                item.owner = owner
                                item.notificationsCount = notificationsCount!!
                                add(item)
                            }
                        }
                    }

                    override fun onCancelled(p0: DatabaseError?) {}
                }
        )
    }
}