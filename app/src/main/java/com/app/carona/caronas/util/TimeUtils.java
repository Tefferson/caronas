package com.app.carona.caronas.util;

import android.content.Context;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Jackelyn on 12/8/2017.
 */

public class TimeUtils {

    public static String format(Context context, Integer hour, Integer minute) {

        String timeDescription = hour.toString() + ":" + minute.toString();
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        Date date = null;

        try {
            date = parseFormat.parse(timeDescription);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(context);
        return timeFormat.format(date);
    }

    public static String getFriendlyDiff(Long diff) {
        Long diffDay = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        if (diffDay > 0) {
            return diffDay.toString() + "d";
        }

        Long diffHour = TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS);
        if (diffHour > 0) {
            return diffHour.toString() + "h";
        }

        Long diffMin = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
        if (diff > 0) {
            return diffMin.toString() + "m";
        }

        Long diffSec = TimeUnit.SECONDS.convert(diff, TimeUnit.MILLISECONDS);
        return diffSec.toString() + "s";
    }
}
