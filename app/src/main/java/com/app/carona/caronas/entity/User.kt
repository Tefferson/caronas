package com.app.carona.caronas.entity

/**
 * Created by teffe on 25/11/2017.
 */
class User {

    var name: String? = null
    var photoUri: String? = null
    var addresses = ArrayList<Address>()
    var vehicles = ArrayList<Vehicle>()
    var contacts = ArrayList<Contact>()
    var isActive = false

    companion object {
        var currentUserId: String? = null
        var currentUser: User? = null
    }
}