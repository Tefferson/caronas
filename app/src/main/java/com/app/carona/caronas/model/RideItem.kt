package com.app.carona.caronas.model

import java.io.Serializable

/**
 * Created by tefferson.guterres on 06/12/2017.
 */
class RideItem : Serializable {

    var rideId: String? = null

    var userId: String? = null

    var userName: String? = null

    var seatsNumber: Int = 0

    var destiny: String? = null

    var origin: String? = null

    var arrivalHour: Int? = null

    var arrivalMinute: Int? = null

    var groupName: String? = null

    var participants: HashMap<String, Boolean>? = null

}