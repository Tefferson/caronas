package com.app.carona.caronas.entity

import java.util.*

/**
 * Created by teffe on 25/11/2017.
 */
class GroupSolicitation(date: Date,
                        isAccepted: Boolean,
                        from: User,
                        to: Group)
    : Solicitation(date, isAccepted) {
}