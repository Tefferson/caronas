package com.app.carona.caronas.entity

import java.io.Serializable

/**
 * Created by teffe on 25/11/2017.
 */
class Vehicle(var idUser: String,
              var plateNumber: String,
              var type: VehicleType,
              var description: String) {
}