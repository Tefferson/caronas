package com.app.carona.caronas.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import com.app.carona.caronas.LoginActivity
import com.app.carona.caronas.R
import com.app.carona.caronas.entity.User
import com.app.carona.caronas.enum.MainPager
import com.app.carona.caronas.fragment.CreateGroupFragment
import com.app.carona.caronas.fragment.CreateRideOfferFragment
import com.app.carona.caronas.fragment.RideDetailFragment
import com.app.carona.caronas.model.GroupItem
import com.app.carona.caronas.model.RideItem
import com.app.carona.caronas.model.SearchGroupItem
import com.app.carona.caronas.util.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView


/**
 * Created by tefferson.guterres on 28/11/2017.
 */
class MainPagerAdapter(private val context: Context,
                       private val viewPager: ViewPager,
                       private val activity: AppCompatActivity)
    : PagerAdapter(), View.OnClickListener, SearchView.OnQueryTextListener {

    private val searchGroupsAdapter by lazy { SearchGroupsAdapter(context, ArrayList<SearchGroupItem>()) }

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean = view === `object`
    override fun getCount(): Int = MainPager.values().size

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        var mainPager = MainPager.values()[position]

        val layout = LayoutInflater.from(context)
                .inflate(mainPager.layoutResId, container, false)

        container?.addView(layout)

        when (mainPager.layoutResId) {
            R.layout.view_activity -> initActivityElements(layout)
            R.layout.view_groups -> initGroupsElements(layout)
            R.layout.view_profile -> initProfileElements(layout)
            R.layout.view_search -> initSearchElements(layout)
        }

        return layout
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.bt_create_group -> openCreateGroupFragment()
            R.id.bt_logout -> doLogout()
            R.id.bt_offer_ride -> openCreateRideOfferFragment()
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (viewPager.currentItem != MainPager.SEARCH.ordinal) {
            viewPager.currentItem = MainPager.SEARCH.ordinal
        }

        searchGroupsAdapter.clear()

        val query = Firebase.groupRef
                .orderByChild("name")
                .startAt(query)

        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                for (data in dataSnapshot?.children!!) {
                    val item = data.getValue(SearchGroupItem::class.java)
                    val member = data?.child("members")?.child(User.currentUserId)?.value
                    if (item?.owner != User.currentUserId && member == null) {
                        item?.peopleCount = data?.child("members")?.childrenCount!!
                        searchGroupsAdapter.add(item)
                    }
                }
            }
        })

        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

    private fun openCreateRideOfferFragment() {
        val fragment = CreateRideOfferFragment()
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.view_activity, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container!!.removeView(`object` as View)
    }

    private fun doLogout() {
        FirebaseAuth.getInstance().signOut()
        activity.startActivity(Intent(activity, LoginActivity::class.java))
        activity.finish()
    }

    private fun openCreateGroupFragment() {
        val fragment = CreateGroupFragment()
        val transaction = activity.fragmentManager.beginTransaction()
        transaction.replace(R.id.view_groups, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun initSearchElements(layout: View?) {
        val lvGroups = layout?.findViewById<ListView>(R.id.lv_available_groups)
        lvGroups?.adapter = searchGroupsAdapter
    }

    private fun initGroupsElements(layout: View?) {

        val lvGroups = layout?.findViewById<ListView>(R.id.lv_groups)

        val items = ArrayList<GroupItem>()
        val groupsAdapter = GroupsAdapter(context, items, activity)
        lvGroups?.adapter = groupsAdapter

        val mQuery = Firebase.userRef.child(User.currentUserId).child("groups").orderByValue()
        mQuery.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {}
            override fun onChildRemoved(dataSnapshot: DataSnapshot?) {
                val groupItem = GroupItem()
                groupItem.name = dataSnapshot?.key!!
                groupItem.notificationsCount = dataSnapshot?.child("solicitations").childrenCount
                groupItem.membersCount = dataSnapshot?.child("members").childrenCount
                groupsAdapter.remove(groupItem)
            }

            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                loadGroups(groupsAdapter, dataSnapshot.key)
            }
        })

        val btCreateGroup by lazy { layout?.findViewById<Button>(R.id.bt_create_group) }
        btCreateGroup?.setOnClickListener(this)
    }

    private fun loadGroups(groupsAdapter: GroupsAdapter, groupName: String?) {
        val query = Firebase.groupRef.child(groupName).orderByValue()
        query.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val item = dataSnapshot.getValue(GroupItem::class.java)
                item?.name = dataSnapshot.key.toString()
                item?.notificationsCount = dataSnapshot.child("solicitations").childrenCount
                item?.membersCount = dataSnapshot.child("members").childrenCount
                groupsAdapter.remove(item)
                groupsAdapter.add(item)
            }
        })

    }

    private fun initProfileElements(layout: View?) {
        val btLogout = layout?.findViewById<Button>(R.id.bt_logout)
        btLogout?.setOnClickListener(this)

        val profileImage = layout?.findViewById<CircleImageView>(R.id.profile_image)
        val tvDisplayName = layout?.findViewById<TextView>(R.id.tv_display_name)

        val query = Firebase.userRef.child(User.currentUserId).orderByValue()
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onDataChange(dataSnapshot: DataSnapshot?) {

                val item = dataSnapshot?.getValue(User::class.java)

                tvDisplayName?.text = item?.name

                Picasso.with(context)
                        .load(item?.photoUri)
                        .into(profileImage)
            }
        })
    }

    private fun initActivityElements(layout: View?) {
        val lvMyRides = layout?.findViewById<ListView>(R.id.lv_my_rides)
        val adapter = RideAdapter(context, ArrayList<RideItem>())
        lvMyRides?.adapter = adapter
        lvMyRides?.setOnItemClickListener { _, _, position, _ ->
            val item = adapter.getItem(position)
            showRideParticipants(item)
        }
        loadRides(adapter)
    }

    private fun showRideParticipants(item: RideItem?) {
        val bundle = Bundle()
        bundle.putSerializable("RIDE_ITEM", item)
        val fragment = RideDetailFragment()
        fragment.arguments = bundle
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.view_activity, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun loadRides(adapter: RideAdapter) {
        val query = Firebase.userRef.child(User.currentUserId).child("groups").orderByValue()
        query.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {}
            override fun onChildAdded(dataSnapshot: DataSnapshot?, p1: String?) {
                loadRides(dataSnapshot?.key, adapter)
            }

            override fun onChildRemoved(p0: DataSnapshot?) {}
        })
    }

    private fun loadRides(groupName: String?, adapter: RideAdapter) {
        val query = Firebase.groupRef.child(groupName).child("rides").orderByValue()
        query.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {}
            override fun onChildAdded(dataSnapshot: DataSnapshot?, p1: String?) {

                val item = dataSnapshot?.getValue(RideItem::class.java)
                item?.groupName = groupName
                item?.rideId = dataSnapshot?.key

                val isOwner = item?.userId == User.currentUserId

                if (isOwner || dataSnapshot?.child("participants")!!.hasChild(User.currentUserId)) {
                    adapter.add(item)
                }
            }

            override fun onChildRemoved(p0: DataSnapshot?) {}
        })
    }
}