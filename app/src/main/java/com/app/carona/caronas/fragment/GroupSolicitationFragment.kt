package com.app.carona.caronas.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.app.carona.caronas.R
import com.app.carona.caronas.adapter.GroupSolicitationAdapter
import com.app.carona.caronas.model.GroupItem
import com.app.carona.caronas.model.GroupSolicitationItem
import com.app.carona.caronas.util.Firebase
import com.app.carona.caronas.util.TimeUtils
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class GroupSolicitationFragment() : Fragment() {
    private var groupItem: GroupItem? = null
    private var lvGroupSolicitation: ListView? = null
    private val items = ArrayList<GroupSolicitationItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        groupItem = arguments.getSerializable("GROUP_ITEM") as GroupItem
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val layout = inflater!!.inflate(R.layout.fragment_group_solicitation, container, false)

        initElements(layout)

        return layout
    }

    private fun initElements(layout: View?) {
        lvGroupSolicitation = layout?.findViewById(R.id.lv_group_solicitation)
        val adapter = GroupSolicitationAdapter(activity, items)
        lvGroupSolicitation?.adapter = adapter
        loadSolicitations(adapter)
    }

    private fun loadSolicitations(adapter: GroupSolicitationAdapter) {
        val query = Firebase.groupRef.child(groupItem?.name).child("solicitations").orderByValue()
        query.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {}
            override fun onChildAdded(dataSnapshot: DataSnapshot?, p1: String?) {
                val date = dataSnapshot?.child("date")?.getValue(Date::class.java)
                loadUserInfo(dataSnapshot?.key, date!!, adapter)
            }

            override fun onChildRemoved(p0: DataSnapshot?) {}
        })
    }

    private fun loadUserInfo(userId: String?, date: Date, adapter: GroupSolicitationAdapter) {
        val query = Firebase.userRef.child(userId).orderByValue()
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {

                val item = dataSnapshot?.getValue(GroupSolicitationItem::class.java)
                item?.userId = dataSnapshot?.key
                item?.groupName = groupItem?.name

                val diff = Date().getTime() - date.getTime()
                item?.elapsedTime = TimeUtils.getFriendlyDiff(diff)

                adapter.add(item)
            }
        })
    }
}