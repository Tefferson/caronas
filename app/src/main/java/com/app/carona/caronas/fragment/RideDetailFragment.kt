package com.app.carona.caronas.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView

import com.app.carona.caronas.R
import com.app.carona.caronas.adapter.ParticipantAdapter
import com.app.carona.caronas.entity.User
import com.app.carona.caronas.model.ParticipantItem
import com.app.carona.caronas.model.RideItem
import com.app.carona.caronas.util.Firebase
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener


/**
 * A simple [Fragment] subclass.
 */
class RideDetailFragment : Fragment() {

    private var rideItem: RideItem? = null
    private var layout: View? = null
    private val lvParticipants by lazy { layout?.findViewById<ListView>(R.id.lv_participants) }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        layout = inflater!!.inflate(R.layout.fragment_ride_detail, container, false)

        rideItem = arguments.getSerializable("RIDE_ITEM") as RideItem

        initElements()

        return layout
    }

    private fun initElements() {
        val adapter = ParticipantAdapter(context, ArrayList<ParticipantItem>())
        lvParticipants?.adapter = adapter
        loadParticipants(adapter)
        loadOwnerInfo(adapter)
    }

    private fun loadParticipants(adapter: ParticipantAdapter) {
        val query = Firebase.groupRef
                .child(rideItem?.groupName)
                .child("rides")
                .child(rideItem?.rideId)
                .child("participants")
                .orderByValue()

        query.addChildEventListener(object : ChildEventListener {

            override fun onCancelled(p0: DatabaseError?) {}
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {}
            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {}
            override fun onChildAdded(dataSnapshot: DataSnapshot?, p1: String?) {
                loadUserInfo(adapter, dataSnapshot?.key)
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot?) {}
        })
    }

    private fun loadOwnerInfo(adapter: ParticipantAdapter) {
        val query = Firebase.userRef.child(rideItem?.userId).orderByValue()
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val user = dataSnapshot?.getValue(User::class.java)
                val item = ParticipantItem()
                item.username = user?.name
                item.photoUri = user?.photoUri
                item.isOwner = true
                adapter.add(item)
            }
        })
    }

    private fun loadUserInfo(adapter: ParticipantAdapter, userId: String?) {
        val query = Firebase.userRef.child(userId).orderByValue()
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {}
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                val user = dataSnapshot?.getValue(User::class.java)
                val item = ParticipantItem()
                item.username = user?.name
                item.photoUri = user?.photoUri
                adapter.add(item)
            }
        })
    }

}
