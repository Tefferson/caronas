package com.app.carona.caronas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.app.carona.caronas.R
import com.app.carona.caronas.model.GroupDetailRidesItem
import com.app.carona.caronas.model.RideItem
import com.app.carona.caronas.util.Firebase
import com.app.carona.caronas.util.TimeUtils
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

/**
 * Created by teffe on 07/12/2017.
 */
class GroupDetailRidesAdapter(context: Context, items: ArrayList<GroupDetailRidesItem>) : ArrayAdapter<GroupDetailRidesItem>(context, R.layout.item_group_detail_rides, items) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val layout = LayoutInflater.from(context)
                .inflate(R.layout.item_group_detail_rides, parent, false)

        initElements(layout, position)

        return layout
    }

    private fun initElements(layout: View?, position: Int) {

        val item = getItem(position)

        val tvRideDestiny = layout?.findViewById<TextView>(R.id.tv_igdr_ride_destiny)
        val tvRideOrigin = layout?.findViewById<TextView>(R.id.tv_igdr_ride_origin)
        val tvRideIntendedArrivalTime = layout?.findViewById<TextView>(R.id.tv_igdr_ride_intended_arrival_time)
        val tvPeopleCount = layout?.findViewById<TextView>(R.id.tv_igdr_ride_people_count)

        val query = Firebase.groupRef.child(item.groupName).child("rides").child(item.rideId).orderByValue()

        query.addValueEventListener(object : ValueEventListener {

            override fun onCancelled(p0: DatabaseError?) {}

            override fun onDataChange(dataSnapshot: DataSnapshot?) {

                val rideData = dataSnapshot?.getValue(RideItem::class.java)
                val seatsOccupied = rideData?.participants?.count() ?: 0

                tvPeopleCount?.text = "$seatsOccupied/${rideData?.seatsNumber}"
            }
        })

        tvRideDestiny?.text = item.destiny
        tvRideOrigin?.text = item.origin
        tvRideIntendedArrivalTime?.text = TimeUtils.format(tvRideIntendedArrivalTime?.context, item.arrivalHour, item.arrivalMinute)
    }

}