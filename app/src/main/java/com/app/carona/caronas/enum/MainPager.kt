package com.app.carona.caronas.enum

import com.app.carona.caronas.R

/**
 * Created by tefferson.guterres on 28/11/2017.
 */
enum class MainPager(val titleResId: Int, val layoutResId: Int, val itemId: Int) {
    GROUPS(R.string.meus_grupos, R.layout.view_groups, R.id.action_groups),
    ACTIVITY(R.string.atividade, R.layout.view_activity, R.id.action_activity),
    PROFILE(R.string.perfil, R.layout.view_profile, R.id.action_profile),
    SEARCH(R.string.buscar, R.layout.view_search, R.id.action_search)
}